//Variables del proceso
def tagImageQA
def tagImagePROD
def artifactName
def artifactVersion
def nameJar
def isRelease
def sonarName = ''
def branchName = ''



pipeline {
    options {
        disableConcurrentBuilds()
    }

    agent {
        label "maven"
    }

    environment {

  		// Artifact info.
  		portActuatorHealthCheck = '8080'
        namespace_qa = 'smart-consignment-qa'
        namespace_prod = 'smart-consignment-prod'
        repoDevOps = 'https://gitlab.com/motato.luis/git-ops-lab.git'
        idFileMavenSettings = '90e6ef21-688d-4969-8a45-319836543bea'
        msName = "ms-smart-consignment"

        //Sonar Configuration
        exclusionesSonar = ' '

  	}

    stages {
        stage("Checkout Source Code") {
            when{
                anyOf {
                    branch 'developer'
                    branch 'feature/*'
                    branch 'main'
                }
            }
            steps {
                echo "Init Checkout Source Code"
                checkout scm
                script {
                    echo "JobName: ${env.JOB_NAME}"
                    sh "env"
                    branchName = "${env.BRANCH_NAME}"
                    sonarName = "${msName}-" +branchName.replaceAll('/','-')
                    echo "sonarName ${sonarName}"

                    //Obtener version del artefacto
                    def pom = readMavenPom file: 'pom.xml'
                    tagImageDEV = pom.version
                    tagImageQA = pom.version + "-" + currentBuild.number
                    tagImagePROD = pom.version + "-" + currentBuild.number
                    artifactName = pom.artifactId
                    artifactVersion = pom.version
                    nameJar = artifactName + "-" + artifactVersion + ".jar"

                    //Identificar si es snapshot o release
                    isRelease = !artifactVersion.contains ('SNAPSHOT')
                    if (isRelease){
                        echo "Es version release: "+ artifactVersion
                    }else{
                        echo "Es version Snapshot: "+ artifactVersion
                    }

                }
                echo "end Checkout Source Code"
            }
        }


        stage("Build") {
            when{
                anyOf {
                    branch 'developer'
                    branch 'feature/*'
                    branch 'main'
                }
            }
            steps {
                echo "Init Build"
                configFileProvider([configFile(fileId: "${idFileMavenSettings}", variable: 'MavenSettings')]) {
                    sh "mvn clean package -DskipTests -s $MavenSettings -U"
                }
                echo "End Build"
            }
        }

        stage("Unit Test") {
            when{
                anyOf {
                    branch 'developer'
                    branch 'feature/*'
                }
            }
            steps {
                echo "Init Unit Test"
                configFileProvider([configFile(fileId: "${idFileMavenSettings}", variable: 'MavenSettings')]) {
                    sh "mvn test package -s $MavenSettings -U"
                }
                echo "End Unit Test"
            }
        }

        stage('SonarQube Scan') {
            when{
                anyOf {
                    branch 'developer'
                    branch 'feature/*'
                }
            }
            steps {
                withSonarQubeEnv('sonar') {
                    configFileProvider([configFile(fileId: "${idFileMavenSettings}", variable: 'MavenSettings')]) {
                        sh "mvn sonar:sonar " +
                        "-Dsonar.java.coveragePlugin=jacoco -Dsonar.junit.reportsPath=target/surefire-reports " +
                        "-Dsonar.exclusions='${exclusionesSonar}' " +
                        "-Dsonar.projectKey=${sonarName} -Dsonar.projectName='${sonarName}' " +
                        "-Dsonar.coverage.jacoco.xmlReportPaths=target/site/jacoco/jacoco.xml -s $MavenSettings -U "
                    }
                }

                sleep(10)
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }



        stage("Config QA Environment") {
            when {
                anyOf {
                    branch 'developer'
                }
            }
            steps {
                script {
            	    openshift.withCluster() {
            		    openshift.withProject("${namespace_qa}") {
            		        dir('gitops') {
            		            git  branch: 'main', credentialsId: 'gitlabaunalab', url: "${repoDevOps}"
            		        }
            		        result_apply_resources = openshift.raw(" apply -f ./gitops/${msName}/qa/ ")
            		        result_apply_template = openshift.apply(openshift.raw("process -f ./gitops/${msName}/template-ms-fuse.yml -p APPLICATION_NAME=${msName} -p ACTUATOR_PORT=${portActuatorHealthCheck} -p PATH_HEALTHCHECK='/actuator/health' ").actions[0].out)
                        }
                    }
                }
            }
        }


        stage("Build Image QA") {
            when {
                anyOf {
                    branch 'developer'
                }
            }
            steps {
                buildImage("${namespace_qa}", "${tagImageQA}", "${msName}", "${nameJar}")
            }
        }

        stage("Deploy QA") {
            when {
                anyOf {
                    branch 'developer'
                }
            }
            steps {
                deployImage("${namespace_qa}", "${tagImageQA}", "${msName}")
            }
        }

        stage("Config PROD Environment") {
            when {
                anyOf {
                    branch 'main'
                }
            }
            steps {
                script {
            	    openshift.withCluster() {
            		    openshift.withProject("${namespace_prod}") {
            		        dir('gitops') {
            		            git  branch: 'main', credentialsId: 'gitlabaunalab', url: "${repoDevOps}"
            		        }
                            result_apply_resources = openshift.raw(" apply -f ./gitops/${msName}/prod/ ")
                            result_apply_template = openshift.apply(openshift.raw("process -f ./gitops/${msName}/template-ms-fuse.yml -p APPLICATION_NAME=${msName} -p ACTUATOR_PORT=${portActuatorHealthCheck} -p PATH_HEALTHCHECK='/actuator/health' ").actions[0].out)
                        }
                    }
                }
            }
        }

        stage("Build Image PROD") {
            when {
                anyOf {
                    branch 'main'
                }
            }
            steps {
                buildImage("${namespace_prod}", "${tagImagePROD}", "${msName}", "${nameJar}")
            }
        }

        stage("Deploy PROD") {
            when {
                anyOf {
                    branch 'main'
                }
            }
            steps {
                deployImage("${namespace_prod}", "${tagImagePROD}", "${msName}")
            }
        }
    }
}

/*
Funcion que permite hacer el build de una imagen a partir del jar generado
*/
def buildImage(actual_namespace, tagImage, msName, nameJar) {
    script {
                    echo "Init Build Image"
                    openshift.withCluster() {
                        openshift.verbose()
                        openshift.withProject("${actual_namespace}") {
                            openshift.selector("bc", "${msName}").startBuild("--from-file=./target/${nameJar}", "--wait=true")
                            openshift.tag("${msName}:latest", "${msName}:${tagImage}")
                        }
                    }
                    echo "End Build Image"
                }
}

/*
Funcion que permite hacer el deploy de una imagen
*/
def deployImage(actual_namespace, tagImage, msName){
    script {
        openshift.withCluster() {
            openshift.verbose()
            openshift.withProject("${actual_namespace}") {
                openshift.set("image", "dc/${msName}", "${msName}=${actual_namespace}/${msName}:${tagImage}", " --source=imagestreamtag")
                openshift.selector("dc", "${msName}").rollout().latest();
                sleep 2

                // Wait for application to be deployed
                def dc = openshift.selector("dc", "${msName}").object()
                def dc_version = dc.status.latestVersion
                def rc = openshift.selector("rc", "${msName}-${dc_version}").object()
                echo "Waiting for ReplicationController ${msName}-${dc_version} to be ready"

                def countIterMax = 20
                def countInterActual = 0
                while (rc.spec.replicas != rc.status.readyReplicas) {
                    sleep 10
                    rc = openshift.selector("rc", "${msName}-${dc_version}").object()
                    countInterActual = countInterActual + 1
                    if (countInterActual > countIterMax) {
                        echo "Se ha superado el tiempo de espera para el despliegue"
                        echo "Se procede a cancelar el despliegue y a mantener la última versión estable"
                        openshift.selector("dc", "${msName}").rollout().cancel();
                        throw new Exception("Se ha superado el tiempo de espera para el despliegue")
                    }
                }
            }
        }
    }
    echo "End Deploy Image"
}




