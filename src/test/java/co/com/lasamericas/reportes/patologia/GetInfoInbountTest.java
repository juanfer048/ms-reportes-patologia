package co.com.lasamericas.reportes.patologia;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.BeanInject;
import org.apache.camel.CamelContext;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@Configuration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, properties = { "server.port=8089" })
public class GetInfoInbountTest {
	
	@Value("${server.port}")
	private String serverPort;

	private static final String URL = "http://localhost:";

	@Autowired
	private TestRestTemplate restTemplate;

	@BeanInject
	private CamelContext camelContext;

	private static final Logger LOG = LoggerFactory.getLogger(GetInfoInbountTest.class);
	
	@Before
	public void interceptorQueryTopUpsDetails() throws Exception {
		
		camelContext.getRouteDefinition("info-inbount-route").adviceWith(camelContext, new AdviceWithRouteBuilder() {
			@Override
			public void configure() throws Exception {
			// @formatter:off
			interceptSendToEndpoint("direct-vm:info-inbount")
				.skipSendToOriginalEndpoint()
				.log(LoggingLevel.INFO, LOG, "Test Consumiendo InfoInbount")
			.end();
			// @formatter:on
			}
		});
	}
	
	@Test
	public void testInfoInbountOK() throws Exception {
		ResponseEntity<Object> response = sendRequestTest();
		assertNotNull(response.getBody());
		assertEquals(200, response.getStatusCodeValue());
		assertTrue(String.valueOf(response).contains("status=200"));
		assertTrue(String.valueOf(response).contains("message=OK"));
	}
	
	/**
	 * Method in charge of sending the request to the REST Authenticate
	 */
	public ResponseEntity<Object> sendRequestTest() throws Exception {

		Map<String, Object> map = this.buildBody();

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(map);
		String url = URL + serverPort + "/lasAmericas/services/patologia/infoInbount";
		LOG.info("URL: {}", url);
		ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Object.class);
		return response;
	}
	
	private Map<String, Object> buildBody() {
		Map<String, Object> map = new HashMap<>();

		map.put("pa", "509932");

		return map;
	}

}
