package co.com.lasamericas.reportes.patologia.dtos;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InfoInbountResultDTO {
	
	@JsonProperty("Movement")
	private ArrayList<MovementResultDTO> Movement;
	
	@JsonProperty("Reading")
	private ArrayList<ReadingResultDTO> Reading;
	
	public ArrayList<MovementResultDTO> getMovement() {
		return Movement;
	}

	public void setMovement(ArrayList<MovementResultDTO> movement) {
		Movement = movement;
	}

	public ArrayList<ReadingResultDTO> getReading() {
		return Reading;
	}

	public void setReading(ArrayList<ReadingResultDTO> reading) {
		Reading = reading;
	}

}
