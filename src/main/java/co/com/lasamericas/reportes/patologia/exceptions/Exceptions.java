package co.com.lasamericas.reportes.patologia.exceptions;

public class Exceptions extends  Exception {

	private static final long serialVersionUID = 1L;

	public Exceptions(String errorMessage) {
		super(errorMessage);
	}

}