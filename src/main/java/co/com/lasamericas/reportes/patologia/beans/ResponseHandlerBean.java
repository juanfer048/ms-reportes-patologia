package co.com.lasamericas.reportes.patologia.beans;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.UnexpectedTypeException;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import co.com.lasamericas.reportes.patologia.constants.Constants;
import co.com.lasamericas.reportes.patologia.dtos.ResponseDTO;
import co.com.lasamericas.reportes.patologia.exceptions.CustomValidationException;


@Component
public class ResponseHandlerBean {
	@Handler
	public ResponseDTO<String> handler(Exchange exchange) {

		Object exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
		ResponseDTO<String> ResponseDTO = new ResponseDTO<String>();

		if (exception instanceof BeanValidationException) {

			BeanValidationException beanValidationException = (BeanValidationException) exception;

			Set<ConstraintViolation<Object>> listErrors = beanValidationException.getConstraintViolations();
			Iterator<ConstraintViolation<Object>> listError = listErrors.iterator();

			while (listError.hasNext()) {
				ConstraintViolation<Object> constraint = listError.next();
				String message = constraint.getMessage();
				ResponseDTO.setStatus(Constants.CODE_BADREQUEST);
				ResponseDTO.setMessage(message);
			}
		} else if (exception instanceof DataAccessException) {
			
			DataAccessException dataAccessException = (DataAccessException) exception;
			
			ResponseDTO.setStatus(Constants.CODE_BADREQUEST);
			ResponseDTO.setMessage(dataAccessException.getMessage());
		} else if (exception instanceof UnexpectedTypeException) {

			UnexpectedTypeException unexpectedTypeException = (UnexpectedTypeException) exception;

			ResponseDTO.setStatus(Constants.CODE_BADREQUEST);
			ResponseDTO.setMessage("El valor " + unexpectedTypeException.getMessage() + " tiene un formato inválido");
		} else if (exception instanceof InvalidFormatException) {
			
			InvalidFormatException invalidFormatException = (InvalidFormatException) exception;
			
			ResponseDTO.setStatus(Constants.CODE_BADREQUEST);
			ResponseDTO.setMessage("El valor " + invalidFormatException.getValue() + " tiene un formato inválido");
		} else if (exception instanceof CustomValidationException) {

			CustomValidationException customValidationException = (CustomValidationException) exception;

			ResponseDTO.setStatus(customValidationException.getStatus());
			ResponseDTO.setMessage(customValidationException.getMessage());
		} else {
			HttpOperationFailedException e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, HttpOperationFailedException.class);
			if(e != null) {
				String body = e.getResponseBody();
				ObjectMapper mapper = new ObjectMapper();
				try {
					@SuppressWarnings("unchecked")
					ResponseDTO<String> response = mapper.readValue(body, ResponseDTO.class);
					ResponseDTO = response;
					return ResponseDTO;
				} catch (JsonParseException e1) {
					e1.printStackTrace();
				} catch (JsonMappingException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			Exception exceptionMs = (Exception) exception;
			ResponseDTO.setStatus(Constants.CODE_BADREQUEST);
			ResponseDTO.setMessage(exceptionMs.getClass() + " - " + exceptionMs.getMessage());
		}

		return ResponseDTO;
	}
}
