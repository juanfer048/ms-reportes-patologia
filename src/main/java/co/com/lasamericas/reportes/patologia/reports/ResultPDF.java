package co.com.lasamericas.reportes.patologia.reports;

import java.net.MalformedURLException;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.layout.LayoutArea;
import com.itextpdf.layout.layout.LayoutContext;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.properties.Leading;
import com.itextpdf.layout.properties.Property;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.renderer.CellRenderer;
import com.itextpdf.layout.renderer.DocumentRenderer;
import com.itextpdf.layout.renderer.DrawContext;
import com.itextpdf.layout.renderer.IRenderer;
import com.itextpdf.layout.renderer.TableRenderer;
import com.itextpdf.layout.element.Image;

public class ResultPDF {
	
	public static final String IMG = "./src/main/resources/img/labmed.png";
	
	public enum POSITION {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT
    }
	
    protected void manipulatePdf(String dest) throws Exception {
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
        Document doc = new Document(pdfDoc);

        TableHeaderEventHandler handler = new TableHeaderEventHandler(doc);
        pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, handler);

        // Calculate top margin to be sure that the table will fit the margin.
        float topMargin = 20 + handler.getTableHeight();
        doc.setMargins(topMargin, 36, 36, 36);

        for (int i = 0; i < 50; i++) {
            doc.add(new Paragraph("Hello World!"));
        }

        doc.add(new AreaBreak());
        doc.add(new Paragraph("Hello World!"));
        doc.add(new AreaBreak());
        doc.add(new Paragraph("Hello World!"));

        doc.close();
    }
    
    private static class ImageAndPositionRenderer extends CellRenderer {
        private Image img;
        private String content;
        private POSITION position;

        public ImageAndPositionRenderer(Cell modelElement, Image img, String content, POSITION position) {
            super(modelElement);
            this.img = img;
            this.content = content;
            this.position = position;
        }

        // If a renderer overflows on the next area, iText uses #getNextRenderer() method to create a new renderer for the overflow part.
        // If #getNextRenderer() isn't overridden, the default method will be used and thus the default rather than the custom
        // renderer will be created
        @Override
        public IRenderer getNextRenderer() {
            return new ImageAndPositionRenderer((Cell) modelElement, img, content, position);
        }

        @SuppressWarnings("resource")
		@Override
        public void draw(DrawContext drawContext) {
            super.draw(drawContext);

            Rectangle area = getOccupiedAreaBBox();
            img.scaleToFit(area.getWidth(), area.getHeight());

            drawContext.getCanvas().addXObjectFittedIntoRectangle(img.getXObject(), new Rectangle(
                    area.getX() + (area.getWidth() - img.getImageWidth()
                            * (float) img.getProperty(Property.HORIZONTAL_SCALING)) / 2,
                    area.getY() + (area.getHeight() - img.getImageHeight()
                            * (float) img.getProperty(Property.VERTICAL_SCALING)) / 2,
                    img.getImageWidth() * (float) img.getProperty(Property.HORIZONTAL_SCALING),
                    img.getImageHeight() * (float) img.getProperty(Property.VERTICAL_SCALING)));

            drawContext.getCanvas().stroke();

            Paragraph p = new Paragraph(content);
            Leading leading = p.getDefaultProperty(Property.LEADING);

            UnitValue defaultFontSizeUV = new DocumentRenderer(new Document(drawContext.getDocument()))
                    .getPropertyAsUnitValue(Property.FONT_SIZE);

            float defaultFontSize = defaultFontSizeUV.isPointValue() ? defaultFontSizeUV.getValue() : 12f;
            float x;
            float y;
            TextAlignment alignment;
            switch (position) {
                case TOP_LEFT: {
                    x = area.getLeft() + 3;
                    y = area.getTop() - defaultFontSize * leading.getValue();
                    alignment = TextAlignment.LEFT;
                    break;
                }

                case TOP_RIGHT: {
                    x = area.getRight() - 3;
                    y = area.getTop() - defaultFontSize * leading.getValue();
                    alignment = TextAlignment.RIGHT;
                    break;
                }

                case BOTTOM_LEFT: {
                    x = area.getLeft() + 3;
                    y = area.getBottom() + 3;
                    alignment = TextAlignment.LEFT;
                    break;
                }

                case BOTTOM_RIGHT: {
                    x = area.getRight() - 3;
                    y = area.getBottom() + 3;
                    alignment = TextAlignment.RIGHT;
                    break;
                }

                default: {
                    x = 0;
                    y = 0;
                    alignment = TextAlignment.CENTER;
                }
            }

            new Canvas(drawContext.getCanvas(), area).showTextAligned(p, x, y, alignment);
        }
    }
    
    private static class TableHeaderEventHandler implements IEventHandler {
        private Table table;
        private float tableHeight;
        private Document doc;

        public TableHeaderEventHandler(Document doc) {
            this.doc = doc;
            initTable();

            TableRenderer renderer = (TableRenderer) table.createRendererSubTree();
            renderer.setParent(new DocumentRenderer(doc));

            // Simulate the positioning of the renderer to find out how much space the header table will occupy.
            LayoutResult result = renderer.layout(new LayoutContext(new LayoutArea(0, PageSize.A4)));
            tableHeight = result.getOccupiedArea().getBBox().getHeight();
        }

        @SuppressWarnings("resource")
		@Override
        public void handleEvent(Event currentEvent) {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) currentEvent;
            PdfDocument pdfDoc = docEvent.getDocument();
            PdfPage page = docEvent.getPage();
            PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
            PageSize pageSize = pdfDoc.getDefaultPageSize();
            float coordX = pageSize.getX() + doc.getLeftMargin();
            float coordY = pageSize.getTop() - doc.getTopMargin();
            float width = pageSize.getWidth() - doc.getRightMargin() - doc.getLeftMargin();
            float height = getTableHeight();
            Rectangle rect = new Rectangle(coordX, coordY, width, height);

            new Canvas(canvas, rect)
                    .add(table)
                    .close();
        }

        public float getTableHeight() {
            return tableHeight;
        }

        private void initTable()
        {
            table = new Table(3);
            table.useAllAvailableWidth();
            Cell cell1 = new Cell();
            try {
				cell1.setNextRenderer(new ImageAndPositionRenderer(cell1, new Image(ImageDataFactory.create(IMG)),
				        "Top left", POSITION.TOP_RIGHT));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            table.addCell(cell1);
        }
    }

}
