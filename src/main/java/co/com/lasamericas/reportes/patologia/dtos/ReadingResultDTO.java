package co.com.lasamericas.reportes.patologia.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReadingResultDTO {
	
	@JsonProperty("lecfue")
	private String Source;
	
	@JsonProperty("lecord")
	private String Order;
	
	@JsonProperty("leccod")
	private String Procedure;
	
	@JsonProperty("lecorg")
	private String Org;
	
	@JsonProperty("lecsis")
	private String System;
	
	@JsonProperty("leclei")
	private String Read;
	
	@JsonProperty("lecimp")
	private String Print;	
	
	@JsonProperty("lecpos")
	private String Position;
	
	@JsonProperty("leccer")
	private String Certify;

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getOrder() {
		return Order;
	}

	public void setOrder(String order) {
		Order = order;
	}

	public String getProcedure() {
		return Procedure;
	}

	public void setProcedure(String procedure) {
		Procedure = procedure;
	}

	public String getOrg() {
		return Org;
	}

	public void setOrg(String org) {
		Org = org;
	}

	public String getSystem() {
		return System;
	}

	public void setSystem(String system) {
		System = system;
	}

	public String getRead() {
		return Read;
	}

	public void setRead(String read) {
		Read = read;
	}

	public String getPrint() {
		return Print;
	}

	public void setPrint(String print) {
		Print = print;
	}

	public String getPosition() {
		return Position;
	}

	public void setPosition(String position) {
		Position = position;
	}

	public String getCertify() {
		return Certify;
	}

	public void setCertify(String certify) {
		Certify = certify;
	}
	

}
