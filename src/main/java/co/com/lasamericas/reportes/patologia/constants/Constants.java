package co.com.lasamericas.reportes.patologia.constants;

public class Constants {

	private Constants() {
	}
		
	public static final String CODE_OK = "200";
	public static final String CODE_NOCONTENT = "204";
	public static final String CODE_BADREQUEST = "400";
	public static final String CODE_INTERNALSERVERERROR = "500";
	
	public static final String MESSAGE_INIT_ROUTE = "Iniciando servicio";
	public static final String MESSAGE_INIT_ROUTE_BODY = "Iniciando servicio | Body: ${body}";
	public static final String MESSAGE_EXCEPTION = "Se presento una excepción ${exception.class}-${exception.message}-${exception.stacktrace}";
	public static final String MESSAGE_FINISH_SERVICE = "Finalizando servicio";

	/**
	 *impresion de fecha para log
	 * */
	public static final String SPEL_NEW_DATE = "#{new java.util.Date()}";
	
	/**
	 * Definir los posicibles tipos de operacion o nombres de los endpoints
	 * */
	public static final String PROPERTY_VALUE_GETPATIENTS = "getPatients";
	public static final String PROPERTY_VALUE_INFOINBOUNT = "getInfoInbount";


	/*
	 * */
	public static final String PROPERTY_OPERATIONSERVICE = "operationService";
	public static final String PROPERTY_TYPEOPERATION = "type-operation";
	
	/*
	 * */
	public static final String MESSAGE_CONVERT_JSON = "Convirtiendo a JSON";
	
	/**
	 * Constantes para comunicacion interproceso
	 * */
	
	public static final String PROPERTY_STOREPROCEDURENAME = "storeProcedureName";
	public static final String PROPERTY_BDCATALOG = "bd-catalog";
	
	/*Las respuesta del los procedimientos quedan asignadas a la propiedad */
	public static final String RESPONSE_PROCEDURE = "result-store-procedure";
	
	/**
	 * Procedimientos almacenados
	 * */

	public static final String PROCEDURE_GET_PATIENTS="spGetPatients";	
	public static final String PROCEDURE_INFO_INBOUNT="spInfoInbount";	
	

	/**PARAMTROS DE ENTRADA**/
	public static final String PARAM_IN_DOC = "document";
	public static final String PARAM_IN_PAT = "pathology";
	public static final String PARAM_IN_ORD = "pa";
	public static final String PARAM_IN_NOM = "name";
	public static final String PARAM_IN_AP1 = "lastname1";
	public static final String PARAM_IN_AP2 = "lastname2";

}