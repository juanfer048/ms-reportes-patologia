package co.com.lasamericas.reportes.patologia.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FilterInfoDTO {
	
	@JsonProperty("document")
	private String document;
	
	@JsonProperty("pathology")
	private String pathology;
	
	@JsonProperty("pa")
	private String pa;
	
	@JsonProperty("names")
	private String names;
	
	@JsonProperty("lastName1")
	private String lastName1;
	
	@JsonProperty("lastName2")
	private String lastName2;

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getPathology() {
		return pathology;
	}

	public void setPathology(String pathology) {
		this.pathology = pathology;
	}

	public String getPa() {
		return pa;
	}

	public void setPa(String pa) {
		this.pa = pa;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getLastName1() {
		return lastName1;
	}

	public void setLastName1(String lastName1) {
		this.lastName1 = lastName1;
	}

	public String getLastName2() {
		return lastName2;
	}

	public void setLastName2(String lastName2) {
		this.lastName2 = lastName2;
	}

}
