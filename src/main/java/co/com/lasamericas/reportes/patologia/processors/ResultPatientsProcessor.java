package co.com.lasamericas.reportes.patologia.processors;

import java.util.ArrayList;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import co.com.lasamericas.reportes.patologia.constants.Constants;
import co.com.lasamericas.reportes.patologia.dtos.InfoInbountResultDTO;
import co.com.lasamericas.reportes.patologia.dtos.MovementResultDTO;
import co.com.lasamericas.reportes.patologia.dtos.PatientsResultDTO;
import co.com.lasamericas.reportes.patologia.dtos.ReadingResultDTO;
import co.com.lasamericas.reportes.patologia.dtos.ResponseDTO;

@Component
public class ResultPatientsProcessor implements Processor {
		
	@Override
	public void process(Exchange exchange) throws Exception {
				
		switch(exchange.getProperty(Constants.PROPERTY_TYPEOPERATION, String.class)) 
		{ 
			case Constants.PROPERTY_VALUE_GETPATIENTS: 
				@SuppressWarnings("unchecked") Map<String, Object> resultsp = (Map<String, Object>) exchange.getProperty(Constants.RESPONSE_PROCEDURE, Object.class);
				@SuppressWarnings("unchecked") ArrayList<PatientsResultDTO> result = (ArrayList<PatientsResultDTO>) resultsp.get("#result-set-1");
				ResponseDTO<ArrayList<PatientsResultDTO>> response = new ResponseDTO<ArrayList<PatientsResultDTO>>();
				response.setMessage("OK");
				response.setStatus(Constants.CODE_OK);
				response.setResult(result);
				exchange.getOut().setBody(response);
				break; 
			case Constants.PROPERTY_VALUE_INFOINBOUNT: 
				@SuppressWarnings("unchecked") Map<String, Object> resultspinbount = (Map<String, Object>) exchange.getProperty(Constants.RESPONSE_PROCEDURE, Object.class);
				@SuppressWarnings("unchecked") ArrayList<MovementResultDTO> resultmov = (ArrayList<MovementResultDTO>) resultspinbount.get("#result-set-1");
				@SuppressWarnings("unchecked") ArrayList<ReadingResultDTO> resultlect = (ArrayList<ReadingResultDTO>) resultspinbount.get("#result-set-2");
				
				InfoInbountResultDTO resultinbount = new InfoInbountResultDTO();
				resultinbount.setMovement(resultmov);
				resultinbount.setReading(resultlect);
				
				ResponseDTO<InfoInbountResultDTO> responseinbount = new ResponseDTO<InfoInbountResultDTO>();
				responseinbount.setMessage("OK");
				responseinbount.setStatus(Constants.CODE_OK);
				responseinbount.setResult(resultinbount);
				exchange.getOut().setBody(responseinbount);
				break; 
			default:
				break; 
		}		
	}

}
