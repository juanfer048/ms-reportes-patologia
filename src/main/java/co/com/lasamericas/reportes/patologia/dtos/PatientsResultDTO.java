package co.com.lasamericas.reportes.patologia.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PatientsResultDTO {
	
	@JsonProperty("parfue")
	private String Source;
	
	@JsonProperty("parord")
	private String Order;
	
	@JsonProperty("parnit")
	private String Document;
	
	@JsonProperty("parap1")
	private String LastName1;
	
	@JsonProperty("parap2")
	private String LastName2;
	
	@JsonProperty("parnom")
	private String Name;
	
	@JsonProperty("parhis")
	private String History;
	
	@JsonProperty("parsex")
	private String Sex;
	
	@JsonProperty("parnac")
	private String BrithDate;
	
	@JsonProperty("partel")
	private String CellNumber;
	
	@JsonProperty("pardir")
	private String Address;
	
	@JsonProperty("pardep")
	private String Department;
	
	@JsonProperty("parciu")
	private String City;
	
	@JsonProperty("parzon")
	private String Zone;
	
	@JsonProperty("pareps")
	private String Eps;
	
	@JsonProperty("parfec")
	private String DateIn;

	@JsonProperty("pardes")
	private String Description;
	
	@JsonProperty("partdo")
	private String DocumentType;
	
	@JsonProperty("paraut")
	private String Autorization;
	
	@JsonProperty("pardia")
	private String Day;
	
	@JsonProperty("parest")
	private String State;
	
	@JsonProperty("parobs")
	private String Observation;

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getOrder() {
		return Order;
	}

	public void setOrder(String order) {
		Order = order;
	}

	public String getDocument() {
		return Document;
	}

	public void setDocument(String document) {
		Document = document;
	}

	public String getLastName1() {
		return LastName1;
	}

	public void setLastName1(String lastName1) {
		LastName1 = lastName1;
	}

	public String getLastName2() {
		return LastName2;
	}

	public void setLastName2(String lastName2) {
		LastName2 = lastName2;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getHistory() {
		return History;
	}

	public void setHistory(String history) {
		History = history;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getBrithDate() {
		return BrithDate;
	}

	public void setBrithDate(String brithDate) {
		BrithDate = brithDate;
	}

	public String getCellNumber() {
		return CellNumber;
	}

	public void setCellNumber(String cellNumber) {
		CellNumber = cellNumber;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getZone() {
		return Zone;
	}

	public void setZone(String zone) {
		Zone = zone;
	}

	public String getEps() {
		return Eps;
	}

	public void setEps(String eps) {
		Eps = eps;
	}

	public String getDateIn() {
		return DateIn;
	}

	public void setDateIn(String dateIn) {
		DateIn = dateIn;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getDocumentType() {
		return DocumentType;
	}

	public void setDocumentType(String documentType) {
		DocumentType = documentType;
	}

	public String getAutorization() {
		return Autorization;
	}

	public void setAutorization(String autorization) {
		Autorization = autorization;
	}

	public String getDay() {
		return Day;
	}

	public void setDay(String day) {
		Day = day;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getObservation() {
		return Observation;
	}

	public void setObservation(String observation) {
		Observation = observation;
	}
}
