package co.com.lasamericas.reportes.patologia.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MovementResultDTO {
	
	@JsonProperty("movfue")
	private String Source;
	
	@JsonProperty("movord")
	private String Order;
	
	@JsonProperty("movhis")
	private String History;
	
	@JsonProperty("movpro")
	private String Procedure;
	
	@JsonProperty("movcam")
	private String Field;
	
	@JsonProperty("movfec")
	private String Date;
	
	@JsonProperty("movdat")
	private String Value;
	
	@JsonProperty("movtip")
	private String Type;
	
	@JsonProperty("movmem")
	private String Description;
	
	@JsonProperty("movpos")
	private String Position;
	
	@JsonProperty("camdes")
	private String Descripcion;
	
	@JsonProperty("tabnom")
	private String ValueField;

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getOrder() {
		return Order;
	}

	public void setOrder(String order) {
		Order = order;
	}

	public String getHistory() {
		return History;
	}

	public void setHistory(String history) {
		History = history;
	}

	public String getProcedure() {
		return Procedure;
	}

	public void setProcedure(String procedure) {
		Procedure = procedure;
	}

	public String getField() {
		return Field;
	}

	public void setField(String field) {
		Field = field;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getValue() {
		return Value;
	}

	public void setValue(String value) {
		Value = value;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getPosition() {
		return Position;
	}

	public void setPosition(String position) {
		Position = position;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public String getValueField() {
		return ValueField;
	}

	public void setValueField(String valueField) {
		ValueField = valueField;
	}
	
}
