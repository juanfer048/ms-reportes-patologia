package co.com.lasamericas.reportes.patologia.routes;

import javax.validation.UnexpectedTypeException;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import co.com.lasamericas.reportes.patologia.beans.LoggerBean;
import co.com.lasamericas.reportes.patologia.beans.ResponseHandlerBean;
import co.com.lasamericas.reportes.patologia.constants.Constants;
import co.com.lasamericas.reportes.patologia.dtos.FilterInfoDTO;
import co.com.lasamericas.reportes.patologia.dtos.InfoInbountDTO;
import co.com.lasamericas.reportes.patologia.dtos.PatientsResultDTO;
import co.com.lasamericas.reportes.patologia.dtos.ResponseDTO;
import co.com.lasamericas.reportes.patologia.exceptions.CustomValidationException;
import co.com.lasamericas.reportes.patologia.routes.TransformationRoute;

@Component
public class TransformationRoute extends RouteBuilder {
	
	private static final Logger logger = LoggerFactory.getLogger(TransformationRoute.class);

	@Override
	public void configure() throws Exception {
						
		//mapear body de entrada endpoint getPatients FilterInfoDTO
		JsonDataFormat jsonFilter= new JsonDataFormat(JsonLibrary.Jackson);
		jsonFilter.setUnmarshalType(FilterInfoDTO.class);
		
		//mapear body de entrada endpoint getPatients FilterInfoDTO
		JsonDataFormat jsonResultPetients= new JsonDataFormat(JsonLibrary.Jackson);
		jsonResultPetients.setUnmarshalType(PatientsResultDTO.class);
		
		//mapear body de entrada endpoint getPatients FilterInfoDTO
		JsonDataFormat jsonInfoInbount= new JsonDataFormat(JsonLibrary.Jackson);
		jsonInfoInbount.setUnmarshalType(InfoInbountDTO.class);
		
		//mapear body de salida ResponseDTO
		JsonDataFormat jsonResponse= new JsonDataFormat(JsonLibrary.Jackson);
		jsonResponse.setUnmarshalType(ResponseDTO.class);
		
		onException(UnexpectedTypeException.class)
			.handled(true)
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(Constants.CODE_BADREQUEST))
			.log(LoggingLevel.ERROR, logger, LoggerBean.buildLog(Constants.MESSAGE_EXCEPTION))
			.bean(ResponseHandlerBean.class)
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_CONVERT_JSON))
			.marshal().json(JsonLibrary.Jackson)
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog(Constants.MESSAGE_FINISH_SERVICE))
			.removeHeaders("*", Exchange.HTTP_RESPONSE_CODE, Exchange.CONTENT_TYPE)
		.end();
		onException(Exception.class)
			.handled(true)
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(Constants.CODE_INTERNALSERVERERROR))
			.log(LoggingLevel.ERROR, logger, LoggerBean.buildLog(Constants.MESSAGE_EXCEPTION))
			.bean(ResponseHandlerBean.class)
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_CONVERT_JSON))
			.marshal().json(JsonLibrary.Jackson)
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog(Constants.MESSAGE_FINISH_SERVICE))
			.removeHeaders("*", Exchange.HTTP_RESPONSE_CODE, Exchange.CONTENT_TYPE)
		.end();
		onException(CustomValidationException.class)
			.handled(true)
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(Constants.CODE_BADREQUEST))
			.log(LoggingLevel.ERROR, logger, LoggerBean.buildLog(Constants.MESSAGE_EXCEPTION))
			.bean(ResponseHandlerBean.class)
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_CONVERT_JSON))
			.marshal().json(JsonLibrary.Jackson)
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog(Constants.MESSAGE_FINISH_SERVICE))
			.removeHeaders("*", Exchange.HTTP_RESPONSE_CODE, Exchange.CONTENT_TYPE)
		.end();
		onException(DataAccessException.class)
			.handled(true)
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(Constants.CODE_INTERNALSERVERERROR))
			.log(LoggingLevel.ERROR, logger, LoggerBean.buildLog(Constants.MESSAGE_EXCEPTION))
			.bean(ResponseHandlerBean.class)
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_CONVERT_JSON))
			.marshal().json(JsonLibrary.Jackson)
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog(Constants.MESSAGE_FINISH_SERVICE))
			.removeHeaders("*", Exchange.HTTP_RESPONSE_CODE, Exchange.CONTENT_TYPE)
		.end(); 
		
		from("direct-vm:get-patients")
			.streamCaching()
			.routeId("get-patients-route")
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_INIT_ROUTE))
			.unmarshal(jsonFilter) 
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_CONVERT_JSON))
			.to("direct:getPatients")
			.process("resultPatientsProcessor")
			.marshal().json(JsonLibrary.Jackson)
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog(Constants.MESSAGE_FINISH_SERVICE))
			.removeHeaders("*", Exchange.HTTP_RESPONSE_CODE, Exchange.CONTENT_TYPE)
		.end();
		
		from("direct-vm:info-inbount")
			.streamCaching()
			.routeId("info-inbount-route")
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_INIT_ROUTE))
			.unmarshal(jsonInfoInbount) 
			.log(LoggingLevel.DEBUG, logger, LoggerBean.buildLog(Constants.MESSAGE_CONVERT_JSON))
			.to("direct:getInfoInbount")
			.process("resultPatientsProcessor")
			.marshal().json(JsonLibrary.Jackson)
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog(Constants.MESSAGE_FINISH_SERVICE))
			.removeHeaders("*", Exchange.HTTP_RESPONSE_CODE, Exchange.CONTENT_TYPE)
		.end();
		
	}
}
