package co.com.lasamericas.reportes.patologia.exceptions;


import com.fasterxml.jackson.annotation.JsonProperty;


public class CustomValidationException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("status")
	private String status;
	
	
	public CustomValidationException(String errorMessage, String status) {
		super(errorMessage);
		this.setStatus(status);
		
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
