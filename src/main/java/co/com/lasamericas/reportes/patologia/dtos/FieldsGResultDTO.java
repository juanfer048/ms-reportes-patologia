package co.com.lasamericas.reportes.patologia.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FieldsGResultDTO {
	
	@JsonProperty("Campro")
	private String Procedure;
	
	@JsonProperty("Camnro")
	private String Number;
	
	@JsonProperty("Camdes")
	private String Description;
	
	@JsonProperty("camtip")
	private String Type;
	
	@JsonProperty("camobl")
	private String Requiered;
	
	@JsonProperty("camcol")
	private String Column;

	@JsonProperty("camarc")
	private String Table;
	
	@JsonProperty("camdep")
	private String Dependency;
	
	@JsonProperty("camuni")
	private String Uni;
	
	@JsonProperty("camimp")
	private String Print;
	
	@JsonProperty("camvde")
	private String Vde;

	@JsonProperty("camvim")
	private String Vim;
	
	@JsonProperty("camcal")
	private String Cal;

	public String getProcedure() {
		return Procedure;
	}

	public void setProcedure(String procedure) {
		Procedure = procedure;
	}

	public String getNumber() {
		return Number;
	}

	public void setNumber(String number) {
		Number = number;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getRequiered() {
		return Requiered;
	}

	public void setRequiered(String requiered) {
		Requiered = requiered;
	}

	public String getColumn() {
		return Column;
	}

	public void setColumn(String column) {
		Column = column;
	}

	public String getTable() {
		return Table;
	}

	public void setTable(String table) {
		Table = table;
	}

	public String getDependency() {
		return Dependency;
	}

	public void setDependency(String dependency) {
		Dependency = dependency;
	}

	public String getUni() {
		return Uni;
	}

	public void setUni(String uni) {
		Uni = uni;
	}

	public String getPrint() {
		return Print;
	}

	public void setPrint(String print) {
		Print = print;
	}

	public String getVde() {
		return Vde;
	}

	public void setVde(String vde) {
		Vde = vde;
	}

	public String getVim() {
		return Vim;
	}

	public void setVim(String vim) {
		Vim = vim;
	}

	public String getCal() {
		return Cal;
	}

	public void setCal(String cal) {
		Cal = cal;
	}
	
}
