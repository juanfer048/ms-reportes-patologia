package co.com.lasamericas.reportes.patologia.processors;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.com.lasamericas.reportes.patologia.constants.Constants;
import co.com.lasamericas.reportes.patologia.dtos.FilterInfoDTO;
import co.com.lasamericas.reportes.patologia.dtos.InfoInbountDTO;


@Component
public class ExecuteStoreProcedureProcessor  implements Processor{

	@Qualifier("databasedllo")
	@Autowired
	private DataSource dataSourceOne;

	
	@Override
	public void process(Exchange exchange) throws Exception {		
			
		  SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSourceOne)
		  .withProcedureName(exchange.getProperty(Constants.PROPERTY_STOREPROCEDURENAME, String.class))
		  .withCatalogName(exchange.getProperty(Constants.PROPERTY_BDCATALOG,String.class)) 
		  .withoutProcedureColumnMetaDataAccess();
		  
		  Map<String, Object> mapResult = new HashMap<>();
		  //validar la accion a realizar con respecto a enpoint consumido 
		  switch(exchange.getProperty(Constants.PROPERTY_TYPEOPERATION, String.class)) 
		  { 
			  case Constants.PROPERTY_VALUE_GETPATIENTS: 
				  mapResult = this.getPatients(exchange, jdbcCall,Constants.PROPERTY_VALUE_GETPATIENTS);
				  break; 
			  case Constants.PROPERTY_VALUE_INFOINBOUNT: 
				  mapResult = this.getInfoInbount(exchange, jdbcCall,Constants.PROPERTY_VALUE_INFOINBOUNT);
				  break; 
			  default:
				  break; 
		  }
		  
		  //se setea la respuesta dentro del exchange
		  exchange.setProperty(Constants.RESPONSE_PROCEDURE, mapResult);
		 
	}
	

	/**
	 * @author Juan Mejia
	 * @apiNote Consultar ingresos de los pacientes
	 * @version 1.0.0  03/05/2022  Juan Mejia 
	 * @throws JsonProcessingException 
	 * */
	private Map<String, Object> getPatients(Exchange exchange, SimpleJdbcCall jdbcCallParam,String service) throws JsonProcessingException{
		Map<String, Object> response = null;
		SimpleJdbcCall jdbcCall = prepareJdbcCall(jdbcCallParam, service);
		
		FilterInfoDTO requestDTO = exchange.getIn().getBody(FilterInfoDTO.class);
		
		SqlParameterSource param = createParam(exchange, service, requestDTO);
		response = jdbcCall.execute(param);
		return response;
	}
	
	/**
	 * @author Juan Mejia
	 * @apiNote Consultar información relacionada a un ingreso
	 * @version 1.0.0  03/05/2022  Juan Mejia 
	 * @throws JsonProcessingException 
	 * */
	private Map<String, Object> getInfoInbount(Exchange exchange, SimpleJdbcCall jdbcCallParam,String service) throws JsonProcessingException{
		Map<String, Object> response = null;
		SimpleJdbcCall jdbcCall = prepareJdbcCall(jdbcCallParam, service);
		
		InfoInbountDTO requestDTO = exchange.getIn().getBody(InfoInbountDTO.class);
		
		SqlParameterSource param = createParam(exchange, service, requestDTO);
		response = jdbcCall.execute(param);
		return response;
	}
	
	
	/**
	 * @author Juan Mejia
	 * @apiNote Declarar los parametros de entrada y salidad de cada proceso almacenado
	 * @version 1.0.0  03/05/2022 
	 * @param jdbcCall recibe la conexion de base de datos 
	 * @param operationType pasar por referencia el numero de caso a ejecutar para declarar los parametros  
	 * */
	private SimpleJdbcCall prepareJdbcCall(SimpleJdbcCall jdbcCall, String operationType) {
		switch (operationType) {
			case Constants.PROPERTY_VALUE_GETPATIENTS: 
				jdbcCall.addDeclaredParameter(new SqlParameter(Constants.PARAM_IN_DOC, Types.VARCHAR));
				jdbcCall.addDeclaredParameter(new SqlParameter(Constants.PARAM_IN_PAT, Types.VARCHAR));
				jdbcCall.addDeclaredParameter(new SqlParameter(Constants.PARAM_IN_ORD, Types.VARCHAR));
				jdbcCall.addDeclaredParameter(new SqlParameter(Constants.PARAM_IN_NOM, Types.VARCHAR));
				jdbcCall.addDeclaredParameter(new SqlParameter(Constants.PARAM_IN_AP1, Types.VARCHAR));
				jdbcCall.addDeclaredParameter(new SqlParameter(Constants.PARAM_IN_AP2, Types.VARCHAR));
				break;
			case Constants.PROPERTY_VALUE_INFOINBOUNT: 
				jdbcCall.addDeclaredParameter(new SqlParameter(Constants.PARAM_IN_ORD, Types.VARCHAR));
				break;
			default:
				break;
		}
		return jdbcCall;
	}
	
	/**
	 * @author Juan Mejia
	 * @apiNote asignarle el valor a los parametros del procedimiento almacenado 
	 * @version 1.0.0  03/05/2022 
	 * @param jdbcCall recibe la conexion de base de datos 
	 * @param operationType pasar por referencia el numero de caso a ejecutar y asigna los valores para los parametros definidos en prepareJdbcCall 
	 * @throws JsonProcessingException 
	 * */
	private SqlParameterSource createParam(Exchange exchange, String operationType,Object object) throws JsonProcessingException {
		SqlParameterSource param = null;
		switch (operationType) {
			case Constants.PROPERTY_VALUE_GETPATIENTS:
				//castear objeto
				FilterInfoDTO bodyRequest = (FilterInfoDTO) object;
				// @formatter:off
				param = new MapSqlParameterSource()
					.addValue(Constants.PARAM_IN_DOC, bodyRequest.getDocument())
					.addValue(Constants.PARAM_IN_PAT, bodyRequest.getPathology())
					.addValue(Constants.PARAM_IN_ORD, bodyRequest.getPa())
					.addValue(Constants.PARAM_IN_NOM, bodyRequest.getNames())
					.addValue(Constants.PARAM_IN_AP1, bodyRequest.getLastName1())
					.addValue(Constants.PARAM_IN_AP2, bodyRequest.getLastName2());
				// @formatter:on
				break;
			case Constants.PROPERTY_VALUE_INFOINBOUNT:
				//castear objeto
				InfoInbountDTO bodyRequestInbount = (InfoInbountDTO) object;
				// @formatter:off
				param = new MapSqlParameterSource()
					.addValue(Constants.PARAM_IN_ORD, bodyRequestInbount.getPa());
				// @formatter:on
				break;
			default:
				break;
		}
		return param;
	}	
}
