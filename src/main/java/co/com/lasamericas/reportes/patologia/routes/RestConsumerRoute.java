package co.com.lasamericas.reportes.patologia.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import co.com.lasamericas.reportes.patologia.dtos.FilterInfoDTO;
import co.com.lasamericas.reportes.patologia.dtos.InfoInbountDTO;
import co.com.lasamericas.reportes.patologia.dtos.ResponseDTO;

/**
 * A simple Camel REST DSL route that implements the greetings service.
 * 
 */
@Component
public class RestConsumerRoute extends RouteBuilder {
	
	@Value("${camel.component.servlet.mapping.context-path}")
	private String contextPath;
	

	private static final String ReportesPatologia = "reportes-patologia";
	
    @Override
    public void configure() throws Exception {
    	
        /*Configuracion Swagger documentacion de la API */
    	
    	restConfiguration()
    	.component("servlet")
    	.enableCORS(true)
    	.contextPath(contextPath.substring(0,contextPath.length() - 2))
    	.apiContextPath("/api-doc")
    	.apiProperty("api.title", "ms-reportes-patologia")
    	.apiProperty("api.version", "1.0.0");
    	
    	rest()
    	.tag(ReportesPatologia)
    	
    	/**
    	 * @author Juan Mejia
    	 * @apiNote endpoint para consultar ingresos de los pacientes
    	 **/
    	
    	.post("{{service.reportes.patologia.get.patients.uri}}")
	    	.id("get-patients")
	    	.description("{{service.reportes.patologia.get.patients.description}}")
	    	.tag(ReportesPatologia)
	    	.type(FilterInfoDTO.class)
	    	.responseMessage()
			    .code(HttpStatus.OK.value())
			    .message(HttpStatus.OK.name())
			    .responseModel(ResponseDTO.class)
			.endResponseMessage()
			.responseMessage()
				.code(HttpStatus.BAD_REQUEST.value())
				.message(HttpStatus.BAD_REQUEST.name())
				.responseModel(ResponseDTO.class)
			.endResponseMessage()
			.responseMessage()
				.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.message(HttpStatus.INTERNAL_SERVER_ERROR.name())
				.responseModel(ResponseDTO.class)
			.endResponseMessage()
			.responseMessage()
				.code(HttpStatus.NOT_FOUND.value())
				.message(HttpStatus.NOT_FOUND.name())
				.responseModel(ResponseDTO.class)
			.endResponseMessage()
	    	.to("direct-vm:get-patients")
    	
    	.post("{{service.reportes.patologia.info.inbount.uri}}")
	    	.id("info-inbount")
	    	.description("{{service.reportes.patologia.info.inbount.description}}")
	    	.tag(ReportesPatologia)
	    	.type(InfoInbountDTO.class)
	    	.responseMessage()
			    .code(HttpStatus.OK.value())
			    .message(HttpStatus.OK.name())
			    .responseModel(ResponseDTO.class)
			.endResponseMessage()
			.responseMessage()
				.code(HttpStatus.BAD_REQUEST.value())
				.message(HttpStatus.BAD_REQUEST.name())
				.responseModel(ResponseDTO.class)
			.endResponseMessage()
			.responseMessage()
				.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.message(HttpStatus.INTERNAL_SERVER_ERROR.name())
				.responseModel(ResponseDTO.class)
			.endResponseMessage()
			.responseMessage()
				.code(HttpStatus.NOT_FOUND.value())
				.message(HttpStatus.NOT_FOUND.name())
				.responseModel(ResponseDTO.class)
			.endResponseMessage()
    	.to("direct-vm:info-inbount");
    }

}