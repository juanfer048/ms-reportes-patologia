package co.com.lasamericas.reportes.patologia.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDTO<T> {
	
	@JsonProperty("result")
	private T result;
	
	@JsonProperty("status")
	private String status;

	@JsonProperty("message")
	private String message;

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T listResult) {
		this.result = listResult;
	}

}
