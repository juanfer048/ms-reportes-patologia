package co.com.lasamericas.reportes.patologia.configurations;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class DatasourceConfig {
    
    @Autowired
    Environment env;

    @Bean(name = "databasedllo")
    @ConfigurationProperties("databasedllo.datasource")
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        return dataSource;
    }
    
    /*@Bean(name = "datasource2")
    @ConfigurationProperties("database2.datasource")
    public DataSource dataSource2() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        return dataSource;
    }  */  
}
