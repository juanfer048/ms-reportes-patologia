package co.com.lasamericas.reportes.patologia.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import co.com.lasamericas.reportes.patologia.beans.LoggerBean;
import co.com.lasamericas.reportes.patologia.constants.Constants;

@Component
public class ExecuteSPRoute  extends RouteBuilder{


	private static final Logger logger = LoggerFactory.getLogger(ExecuteSPRoute.class);
	
	@Override
	public void configure() throws Exception {
		
		from("direct:getPatients")
			.routeId("getPatients-route")
			.streamCaching()
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog("Ejecutando procedimiento almacenado"))
			.setProperty(Constants.PROPERTY_STOREPROCEDURENAME).simple(Constants.PROCEDURE_GET_PATIENTS)
			.setProperty(Constants.PROPERTY_TYPEOPERATION).simple(Constants.PROPERTY_VALUE_GETPATIENTS)
			.process("executeStoreProcedureProcessor")
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog("Ejecución de procedimiento almacenado exitoso"))
		.end();
		
		from("direct:getInfoInbount")
			.routeId("getInfoInbount-route")
			.streamCaching()
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog("Ejecutando procedimiento almacenado"))
			.setProperty(Constants.PROPERTY_STOREPROCEDURENAME).simple(Constants.PROCEDURE_INFO_INBOUNT)
			.setProperty(Constants.PROPERTY_TYPEOPERATION).simple(Constants.PROPERTY_VALUE_INFOINBOUNT)
			.process("executeStoreProcedureProcessor")
			.log(LoggingLevel.INFO, logger, LoggerBean.buildLog("Ejecución de procedimiento almacenado exitoso"))
		.end();
		
	}

}