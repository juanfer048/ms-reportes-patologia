package co.com.lasamericas.reportes.patologia.beans;

public class LoggerBean {
	

	private LoggerBean() {
	}
	
	public static String buildLog(String message) {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("MicroService: ms-reportes-patologia");
		messageBuilder.append(" | OperationService: ${property.operationService}");
		messageBuilder.append(message);
		return messageBuilder.toString();
	}

}
