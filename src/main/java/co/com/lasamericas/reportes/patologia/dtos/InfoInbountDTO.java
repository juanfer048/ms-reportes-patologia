package co.com.lasamericas.reportes.patologia.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InfoInbountDTO {
	
	@JsonProperty("pa")
	private String pa;

	public String getPa() {
		return pa;
	}

	public void setPa(String pa) {
		this.pa = pa;
	}

}
